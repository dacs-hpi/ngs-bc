def preProcess(df):
    df = df.drop(range(0, 699))
    df = df.dropna(subset=['Qual2'])
    df = df.reset_index(drop=True)
    df['As_Score'] = df['As_Score'].str.replace('AS:i:', '').astype(int)
    df['Read_ID'] = df['Read_ID'].str.replace('read.1_', '')
    df['len'] = df['Sequence'].str.len()
    df = df.drop(labels=['Read_Map', 'Sequence', 'Qual1', 'Qual2', '7', '8'], axis=1)
    df['tag1'] = df['tag1'].str.replace('GE:Z:', '')
    df = df[['Read_ID', 'MAPQ', 'tag1', 'As_Score']]
    return df

