# This code is developed for Read-Level Binary Classification of NGS cycles using Long-short Term Memory neural network.
# The developer and author is Athar Khodabakhsh.

# A detailed breakdown of what the code requires:
# Import the necessary libraries: pandas, numpy, shap, imblearn, matplotlib, keras, sklearn, shap, and tensorflow.
# Also import a custom pre-processing function from preP.py.
import pandas as pd
import numpy as np
import shap
import sns as sns
from imblearn.over_sampling import RandomOverSampler
from pandas import DataFrame
from sklearn import preprocessing
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import matplotlib.pyplot as plt
from keras.layers import Dense, Lambda, Input, Flatten, Embedding
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dropout

from sklearn.metrics import mean_squared_error, r2_score, accuracy_score, precision_score, recall_score, f1_score, \
    confusion_matrix, roc_curve
from sklearn.preprocessing import LabelEncoder
from sklearn import metrics
from sklearn.utils import shuffle

import tensorflow as tf
tf.compat.v1.disable_v2_behavior()

from preP import preProcess

# This code reads and preprocesses genomic data in the form of SAM files. A sliding window schema is used to read
# multiple SAM files and concatenate them into one DataFrame containing three consecutive NGS cycles. It then
# performs feature scaling and encoding before splitting the data into three sets based on the sliding window approach.

# The starting NGS cycle and step size is defined.
step = 10
cycle = 35
i = 1
for i in range(1,9):

    # Read the SAM file using Pandas' read_csv() function. Rename the columns to match the SAM file format, and call the
    # preProcess() function to preprocess the data.
    file_num = str(cycle)
    df1 = pd.read_csv('P3/'+file_num+'.sam', delimiter="\t",
                      names=['Read_ID', '0/16', 'Read_Map', 'Position', 'MAPQ', 'CIGAR', 'len', '7', '8',
                             'Sequence', 'Qual1', 'As_Score', 'Qual2', 'tag1', 'NM', 'tag2'], low_memory=False)
    df1 = preProcess(df1)
    print(cycle, 'read numbers:', len(df1))

    cycle = cycle + step
    file_num = str(cycle)
    df2 = pd.read_csv('P3/'+file_num+'.sam', delimiter="\t",
                      names=['Read_ID', '0/16', 'Read_Map', 'Position','MAPQ', 'CIGAR', 'len', '7', '8',
                             'Sequence', 'Qual1', 'As_Score', 'Qual2', 'tag1', 'NM', 'tag2'], low_memory=False)
    df2 = preProcess(df2)
    print(cycle, 'read numbers:', len(df2))

    cycle = cycle + step
    file_num = str(cycle)
    df3 = pd.read_csv('P3/'+file_num+'.sam', delimiter="\t",
                      names=['Read_ID', '0/16', 'Read_Map', 'Position', 'MAPQ', 'CIGAR', 'len', '7', '8',
                             'Sequence', 'Qual1', 'As_Score', 'Qual2', 'tag1', 'NM', 'tag2'], low_memory=False)
    df3 = preProcess(df3)
    print(cycle, 'read numbers:', len(df3))

    df4 = pd.read_csv('P3/135.sam', delimiter="\t",
                      names=['Read_ID', '0/16', 'Read_Map', 'Position', 'MAPQ', 'CIGAR', 'len', '7', '8',
                             'Sequence', 'Qual1', 'As_Score', 'Qual2', 'tag1', 'NM', 'tag2'], low_memory=False)
    df4 = preProcess(df4)
    print('last cycle read numbers:', len(df4))


    concat = pd.concat([df1[:], df2[:], df3[:], df4[:]])
    print(concat.head())

    # ----------------------
    # 'Read_ID' column contains unique IDs assigned to the reads,
    # while 'tag1' column contains a tag for each read. The dictionary is created by first encoding the 'tag1' column
    # with unique integer values using the pd.factorize() function, and then grouping the encoded values by their
    # respective tag names and counting their frequency.
    labels = concat[['Read_ID', 'tag1']]
    labels['Read_ID'] = pd.factorize(labels['tag1'])[0]+1
    sps = labels.groupby('tag1').size()

    # ----------------------
    # The 'concat' DataFrame is preprocessed for machine learning. First, the 'tag1' and 'Read_ID' columns are encoded
    # using pd.factorize() function, so that the categorical data can be used as  input to machine learning algorithms.
    # Then, the 'tag1' column is normalized using preprocessing.MinMaxScaler() function to a range of 0 to 1,
    # and the encoded 'tag1' column is transformed using LabelEncoder() and divided by its
    # maximum value to get a range of 0 to 1.

    concat['tag1'] = pd.factorize(concat['tag1'])[0]
    concat['Read_ID'] = pd.factorize(concat['Read_ID'])[0]
    count = concat['tag1'].nunique()
    print(count)
    concat.to_numpy()

    scalerT = preprocessing.MinMaxScaler(feature_range=(0, 1))
    scaled = scalerT.fit_transform(concat)
    print(scaled)
    print(scaled.shape)

    encoder = LabelEncoder()
    scaled[:,2] = encoder.fit_transform(scaled[:,2])+1
    scaled[:,2] = scaled[:,2]/max(scaled[:,2])

    #----------------------
    # Preprocessing and labeling. The meta-information obtained from each cycle is used to form the sliding window
    # schema and their corresponding labels (0/1) based on the Binary Classification idea in this study.
    scaled_features = pd.DataFrame(scaled, columns=["ID", "MAPQ", "tag1", "AS"])
    print(scaled_features)

    # Rename columns and split scaled_features into three batches of consecutive cycles collected from each NGS cycle
    # (df#).
    batch1 = scaled_features[:len(df1)].rename(
        columns={"ID": "R1", "MAPQ": "M1", "tag1": "L1", "AS": "A1"})

    batch2 = scaled_features[len(df1):len(df1) + len(df2)].sort_values(by=["ID"], ignore_index=True).rename(
        columns={"ID": "R2", "MAPQ": "M2", "tag1": "L2", "AS": "A2"})

    batch3 = scaled_features[len(df1) + len(df2):len(df1) + len(df2) + len(df3)].sort_values(
        by=["ID"], ignore_index=True).rename(columns={"ID": "R3", "MAPQ": "M3", "tag1": "L3", "AS": "A3"})

    batch4 = scaled_features[len(df1) + len(df2) + len(df3):len(df1) + len(df2) + len(df3) + len(df4)].sort_values(
        by=["ID"], ignore_index=True).rename(columns={"ID": "R4", "MAPQ": "M4", "tag1": "L4", "AS": "A4"})

    # Merge the dataframes together.
    left_merge = batch1.merge(batch2, left_on='R1', right_on='R2', how='outer').fillna(0)
    left_merge = left_merge.merge(batch3, left_on='R2', right_on='R3', how='outer').fillna(0)
    left_merge = left_merge.merge(batch4, left_on='R3', right_on='R4', how='outer').fillna(0)
    merged_data = left_merge

    # Binary label the NGS reads' meta-information processed by merging three consecutive cycles.
    merged_data['y1'] = np.where((merged_data['L1'] == merged_data['L2']) & (merged_data['L2'] == merged_data['L4'])
                                 & (merged_data['L1'] != 0) & (merged_data['L2'] != 0), 1, 0)
    merged_data['y2'] = np.where((merged_data['L1'] == merged_data['L2']) & (merged_data['L2'] == merged_data['L3'])
                                 & (merged_data['L2'] != 0) & (merged_data['L3'] != 0), 1, 0)


    print(merged_data)
    print('train labels', merged_data['y1'].sum(), 'test labels', merged_data['y2'].sum())

    merged_data = shuffle(merged_data)
    merged_data = merged_data.reset_index(drop=True)
    values = merged_data.values

    # ----------------------
    # Split data into training, validation, and test sets. The meta-information is stored in the variable values.
    # 80% of data is used for training, 10% for validation, and 10% for test.
    n_train_time = round(len(values) * 0.8)
    train = values[:n_train_time, [0, 1, 2, 3, 4, 5, 6, 7, 12, 13, 14, 15, 16]]


    # Define oversampling strategy. The minority class (negative class 0s) are oversampled to reduce the over-fitting
    # and biased decisions. The oversampling is performed only on training set.
    oversample = RandomOverSampler(sampling_strategy='minority')
    X_over, y_over = oversample.fit_resample(train[:, [1, 2, 3, 5, 6, 7, 9, 10, 11]], train[:, 12])
    train_X, train_y = X_over, y_over
    valid = values[n_train_time:n_train_time + (round(len(values) * 0.1)), [0, 1, 2, 3, 4, 5, 6, 7, 12, 13, 14, 15, 16]]
    test = values[n_train_time + (round(len(values) * 0.1)):, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 17]]


    #----------------------
    # Prepare validation and test data for Binary classification,
    valid_X, valid_y = valid[:, [1, 2, 3, 5, 6, 7, 9, 10, 11]], valid[:, 12]
    test_X, test_y = test[:, [1, 2, 3, 5, 6, 7, 9, 10, 11]], test[:, 12]

    # Reshape the input to 3D tensor [samples, timesteps, features]
    train_X = train_X.reshape((train_X.shape[0], 1, train_X.shape[1]))
    valid_X = valid_X.reshape((valid_X.shape[0], 1, valid_X.shape[1]))
    test_X = test_X.reshape((test_X.shape[0], 1, test_X.shape[1]))
    print(train_X.shape, train_y.shape, valid_X.shape, valid_y.shape, test_X.shape, test_y.shape)

    #----------------------
    # Define a sequential model in Keras for a binary classification task using LSTM layers.
    binary_classification_model = Sequential()
    binary_classification_model.add(LSTM(100, return_sequences=True, input_shape=(train_X.shape[1], train_X.shape[2])))
    binary_classification_model.add(Dropout(0.2))
    binary_classification_model.add(LSTM(50, return_sequences=True))
    binary_classification_model.add(LSTM(25, return_sequences=False))
    binary_classification_model.add(Dense(1, activation='sigmoid'))

    # Compile the model.
    binary_classification_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['acc'])
    print(binary_classification_model.summary())

    # Train the binary_classification_model.
    history = binary_classification_model.fit(train_X, train_y, epochs=100, batch_size=200,
                                              validation_data=(valid_X, valid_y), verbose=2)
    # Summarize history for loss.
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('binary_classification_model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    # plt.legend(['train', 'test'], loc='upper right')
    # plt.show()

    #----------------------
    # Final evaluation of the binary_classification_model.
    scores = binary_classification_model.evaluate(valid_X, valid_y, verbose=0)


    yhat = binary_classification_model.predict(valid_X)
    y_bool = np.where((yhat>0.9) , 1, 0)
    rmse = np.sqrt(mean_squared_error(valid_y, yhat))
    print('Test RMSE: %.3f' % rmse)

    accuracyV = accuracy_score(valid_y, y_bool)
    precisionV = precision_score(valid_y, y_bool)
    recallV = recall_score(valid_y, y_bool)
    F1 = f1_score(valid_y, y_bool)

    print("Validation Accuracy: {:.2f}".format(accuracyV))
    print("Validation Precision: {:.2f}".format(precisionV))
    print("Validation Recall: {:.2f}".format(recallV))
    print("Validation F1 Score: {:.2f}".format(F1))

    import seaborn as sns
    cm = confusion_matrix(valid_y, y_bool)
    sns.heatmap(cm, annot=True, cmap="Blues", fmt="d")
    plt.title("Confusion Matrix- Validation")
    plt.xlabel("Predicted Label")
    plt.ylabel("True Label")
    plt.show()

    from sklearn.metrics import classification_report
    report = classification_report(valid_y, y_bool)
    print(report)


#______________________________________
    # Binary classification results on test data
    yhat2 = binary_classification_model.predict(test_X)
    y_bool2 = np.where((yhat2>0.9) , 1, 0)
    rmse = np.sqrt(mean_squared_error(test_y, yhat2))
    print('Test RMSE: %.3f' % rmse)

    accuracy = accuracy_score(test_y, y_bool2)
    precision = precision_score(test_y, y_bool2)
    recall = recall_score(test_y, y_bool2)
    f1 = f1_score(test_y, y_bool2)

    print("Test Accuracy: {:.2f}".format(accuracy))
    print("Test Precision: {:.2f}".format(precision))
    print("Test Recall: {:.2f}".format(recall))
    print("Test F1 Score: {:.2f}".format(f1))

    import seaborn as sns
    cm = confusion_matrix(test_y, y_bool2)
    sns.heatmap(cm, annot=True, cmap="Blues", fmt="d")
    plt.title("Confusion Matrix- Test")
    plt.xlabel("Predicted Label")
    plt.ylabel("True Label")
    plt.show()

    print(classification_report(test_y, y_bool2))

    # y_pred_proba = binary_classification_model.predict_proba(test_X)
    # fpr, tpr, _ = metrics.roc_curve(test_y,  yhat2)
    # auc = metrics.roc_auc_score(test_y, yhat2)
    # plt.plot(fpr,tpr,label="data 1, auc="+str(auc))
    # plt.legend(loc=4)
    # plt.show()

    # -----------------------------
    # Shapley evaluation

    # Define constants
    NUM_BACKGROUND_SAMPLES = 100

    if cycle == 85:
        background = train_X[np.random.choice(train_X.shape[0], NUM_BACKGROUND_SAMPLES, replace=False)]
        # Explain model predictions using SHAP's DeepExplainer.
        explainer = shap.DeepExplainer(binary_classification_model, background)
        shap_values = explainer.shap_values(test_X)

        # Visualize SHAP explanations for a single prediction on test data by force_plot.
        shap_explain = shap.force_plot(explainer.expected_value, shap_values[0][1], features=test_X[:1], matplotlib=True)
        # shap_explain = shap.force_plot(explainer.expected_value, shap_values[0][2], features=valid_X[1:2],matplotlib=True)

        # Generate summary_plot of SHAP values on test data.
        names = ['MAPQ-C1', 'PTH_ID-C1', 'As-C1', 'MAPQ-C2', 'PTH_ID-C2', 'As-C2', 'MAPQ-C3', 'PTH_ID-C3',
                 'As-C3']
        features = test_X[:][:].reshape(test_X.shape[0], test_X.shape[2])
        shaps = shap_values[0][:].reshape(test_X.shape[0], test_X.shape[2])
        shap.summary_plot(shaps, features, feature_names=names)

        # -----------------------------
        # Best Binary LSTM classifier is selected by observing model performance based on F1-score evaluation.
        # The best performing model is then used for testing the model's performance on four other patients'
        # data WITHOUT retraining. Then F1-score is monitored for each sample over sliding windows to locate highest
        # performing window therefore identify earliest decision-making time.

        stepT = 10
        cycleT = 35
        for j in range(1, 9):
            file_num = str(cycleT)
            df1T = pd.read_csv('P5/' + file_num + '.sam', delimiter="\t",
                               names=['Read_ID', '0/16', 'Read_Map', 'Position', 'MAPQ', 'CIGAR', 'len', '7', '8',
                                      'Sequence', 'Qual1', 'As_Score', 'Qual2', 'tag1', 'NM', 'tag2'], low_memory=False)
            df1T = preProcess(df1T)
            print(len(df1T))

            cycleT = cycleT + stepT
            file_num = str(cycleT)
            df2T = pd.read_csv('P5/' + file_num + '.sam', delimiter="\t",
                               names=['Read_ID', '0/16', 'Read_Map', 'Position', 'MAPQ', 'CIGAR', 'len', '7', '8',
                                      'Sequence', 'Qual1', 'As_Score', 'Qual2', 'tag1', 'NM', 'tag2'], low_memory=False)
            df2T = preProcess(df2T)
            print(len(df2T))

            cycleT = cycleT + stepT
            file_num = str(cycleT)
            df3T = pd.read_csv('P5/' + file_num + '.sam', delimiter="\t",
                               names=['Read_ID', '0/16', 'Read_Map', 'Position', 'MAPQ', 'CIGAR', 'len', '7', '8',
                                      'Sequence', 'Qual1', 'As_Score', 'Qual2', 'tag1', 'NM', 'tag2'], low_memory=False)
            df3T = preProcess(df3T)
            print(len(df3T))

            # n = int(input ("Enter number :"))
            print('sliding window number:', cycleT - 2 * stepT, cycleT - stepT, cycleT, 135)
            # n = min(len(df1T), len(df2T), len(df3T))
            concatT = pd.concat([df1T[:], df2T[:], df3T[:]])
            print(concatT.head())

            # ----------------------
            # Dictionary building
            labels = concatT[['Read_ID', 'tag1']]
            labels['Read_ID'] = pd.factorize(labels['tag1'])[0] + 1
            sps = labels.groupby('tag1').size()
            # ----------------------

            concatT['tag1'] = pd.factorize(concatT['tag1'])[0]
            concatT['Read_ID'] = pd.factorize(concatT['Read_ID'])[0]
            countp = concatT['tag1'].nunique()
            print(countp)
            concatT.to_numpy()

            scalerT = preprocessing.MinMaxScaler(feature_range=(0, 1))
            scaledT = scalerT.fit_transform(concatT)
            print(scaledT)
            print(scaledT.shape)

            encoder = LabelEncoder()
            scaledT[:, 2] = encoder.fit_transform(scaledT[:, 2]) + 1
            scaledT[:, 2] = scaledT[:, 2] / max(scaledT[:, 2])

            # ----------------------
            # Preprocessing and labeling the test data
            scaled_Test_Features = pd.DataFrame(scaledT)
            scaled_Test_Features = scaled_Test_Features.rename(columns={0: "ID", 1: "MAPQ", 2: "tag1", 3: "AS"})
            print(scaled_Test_Features)

            batch_test1 = scaled_Test_Features[:len(df1T)].rename(
                columns={"ID": "R1", "MAPQ": "M1", "tag1": "L1", "AS": "A1"})
            batch_test2 = scaled_Test_Features[len(df1T): len(df1T) + len(df2T)].sort_values(
                by=['ID'], ignore_index=True).rename(columns={"ID": "R2", "MAPQ": "M2", "tag1": "L2", "AS": "A2"})
            batch_test3 = scaled_Test_Features[len(df1T) + len(df2T):len(df1T) + len(df2T) + len(df3T)].sort_values(
                by=['ID'], ignore_index=True).rename(columns={"ID": "R3", "MAPQ": "M3", "tag1": "L3", "AS": "A3"})

            left_mergeT = batch_test1.merge(batch_test2, left_on='R1', right_on='R2', how='outer').fillna(0)
            left_mergeT = left_mergeT.merge(batch_test3, left_on='R2', right_on='R3', how='outer').fillna(0)
            merged_dataT = left_mergeT

            merged_dataT['y1'] = np.where((merged_dataT['L1'] == merged_dataT['L2']) &
                                          (merged_dataT['L2'] == merged_dataT['L3']) &
                                          (merged_dataT['L2'] != 0) & (merged_dataT['L3'] != 0), 1, 0)
            print(merged_dataT)

            merged_dataT = shuffle(merged_dataT)
            merged_dataT = merged_dataT.reset_index(drop=True)
            values = merged_dataT.values

            testT = values[:, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]]
            testT_X, testT_y = testT[:, [1, 2, 3, 5, 6, 7, 9, 10, 11]], testT[:, 12]
            
            # Reshape the input to 3D tensor [samples, timesteps, features]
            testT_X = testT_X.reshape((testT_X.shape[0], 1, testT_X.shape[1]))
            
            # ----------------------
            # Binary classification result evaluation.
            yhatT = binary_classification_model.predict(testT_X)
            y_boolT = np.where((yhatT > 0.8), 1, 0)
            rmse = np.sqrt(mean_squared_error(testT_y, yhatT))
            print('Test RMSE: %.3f' % rmse)

            print(classification_report(testT_y, y_boolT))

            fpr, tpr, _ = metrics.roc_curve(testT_y, yhatT)
            auc = metrics.roc_auc_score(testT_y, yhatT)
            plt.plot(fpr, tpr, label="data 1, auc=" + str(auc))
            plt.legend(loc=4)
            # plt.show()
            cycleT = cycleT - stepT

            resultTp = testT_X.reshape((testT_X.shape[0], testT_X.shape[2]))
            resultTp[:, [7]] = resultTp[:, [7]] * y_boolT * countp
            resultP = pd.DataFrame(resultTp)

            # Store the prediction results over reads identified as true reads to appear in the final cycle over each
            # window.
            resultP = resultP.sort_values(by=[7], ascending=True)
            resultP = resultP[resultP.iloc[:, 7] > 0]
            sps = labels.groupby('tag1').max()
            sps = pd.DataFrame(sps).reset_index()
            speci_test = resultP.groupby([7]).size()
            speci_test = pd.DataFrame(speci_test).reset_index()
            finalr_rest = speci_test.merge(sps, left_on=7, right_on='Read_ID')
            print(finalr_rest.head())
            speci_test = finalr_rest.sort_values(by=0, ascending=False)
            ot = speci_test.iloc[0:140, 1:3]
            ot.to_csv('outCSV/paperReadyotherP5_'+file_num+'.csv', index=False)
    # #     # -----------------------------


    cycle = cycle - step
